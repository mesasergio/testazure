require('isomorphic-fetch');
const { Client } = require('@microsoft/microsoft-graph-client');
const { TokenCredentialAuthenticationProvider } = require('@microsoft/microsoft-graph-client/authProviders/azureTokenCredentials');
const { ClientSecretCredential } = require('@azure/identity');

const defaultWithMiddlewareOptions = {
    debugLogging: false,
    scopes: ['https://graph.microsoft.com/.default']
};

// https://docs.microsoft.com/en-us/graph/sdks/choose-authentication-providers?tabs=Javascript
class AzureGraphProvider
{
    // API~
    static initWithMiddleware(credential, { scopes, debugLogging } = defaultWithMiddlewareOptions)
    {
        const clientCredential = new ClientSecretCredential(
            credential.tenantId,
            credential.clientId,
            credential.clientSecret
        );
        const authProvider = new TokenCredentialAuthenticationProvider(
            clientCredential,
            { scopes }
        );
        
        return Client.initWithMiddleware({ debugLogging, authProvider });
    }
    
    // (Client.init) For MSAL - needs different authProvider
}

module.exports = AzureGraphProvider;