const defaultConstructorOptions = {
  emailSender: process.env.DEFAULT_EMAIL_SENDER,
};

const defaultUserOptions = {
  accountEnabled: false,
  passwordProfile: {
    forceChangePasswordNextSignIn: true,
  },
};

class AzureGraphService {
  #client;

  #emailSender;

  #tenantName;

  constructor(client, { emailSender, tenantName } = defaultConstructorOptions) {
    this.#client = client;
    this.#emailSender = emailSender;
    this.#tenantName = tenantName;
  }

  /*
    https://docs.microsoft.com/en-us/graph/api/invitation-post?view=graph-rest-1.0&tabs=http
    {
        invitedUserEmailAddress: 'user@email.com',
        inviteRedirectUrl: 'http://localhost:3000'
    }
  */
  async invite(invitation) {
    return this.#client.api('/invitations').post(invitation);
  }

  /*
    {
      subject: '<Subject>',
      body: {
        contentType: 'HTML',
        content: '<Content>'
      },
      toRecipients: [
          {
            emailAddress: {
              address: '<user@email.com>'
            }
          }
      ]
    }
  */
  async sendMail(message, emailSender = this.#emailSender) {
    return this.#client.api(`/users/${emailSender}/sendMail`).post({ message });
  }

  /*
    https://docs.microsoft.com/en-us/graph/api/user-post-users?view=graph-rest-1.0&tabs=javascript
    {
        accountEnabled: true,
        displayName: 'John Smith',
        mailNickname: 'JohnSmith', // clean string
        userPrincipalName: 'user@tenant.onmicrosoft.com',
        passwordProfile: {
            forceChangePasswordNextSignIn: true,
            password: 'A9#+++++'
        },
        identities?: [
            {
                signInType: 'userName', // emailAddress federated
                issuer: 'tenant.onmicrosoft.com', // facebook.com
                issuerAssignedId: 'johnsmith'
            }
        ],
        passwordPolicies?: 'DisablePasswordExpiration'
    }
  */
  async createTenantUser(userInfo) {
    const user = { ...defaultUserOptions, ...userInfo };

    const userPrincipalName = this.generatePrincipalName(
      user.userPrincipalName,
    );

    let password;
    if (!user.passwordProfile.password) {
      password = AzureGraphService.generatePassword();
    } else if (
      !AzureGraphService.isValidPassword(user.passwordProfile.password)
    ) {
      throw new Error('Bad password format');
    }

    const newUser = {
      accountEnabled: user.accountEnabled,
      displayName: user.displayName,
      mailNickname: AzureGraphService.fixMailNickname(user.mailNickname),
      principalName: user.principalName,
      userPrincipalName,
      passwordProfile: {
        forceChangePasswordNextSignIn:
          user.passwordProfile.forceChangePasswordNextSignIn,
        password,
      },
    };

    return this.#client.api('/users').post(newUser);
  }

  async createSocialAndLocalUser({ displayName, email }) {
    const password = AzureGraphService.generatePassword();

    const domainName = AzureGraphService.extractDomainName(email);

    const newUser = {
      displayName,
      accountEnabled: true,
      passwordProfile: {
        password,
        forceChangePasswordNextSignIn: false,
      },
      identities: [
        {
          SignInType: 'federated',
          Issuer: domainName,
          IssuerAssignedId: AzureGraphService.generateRandomIdentifier(),
        },
        {
          signInType: 'emailAddress',
          issuer: this.domainName,
          issuerAssignedId: email,
        },
      ],
      passwordPolicies: 'DisablePasswordExpiration',
    };

    return this.#client.api('/users').post(newUser);
  }

  async createLocalUser({ displayName, email }) {
    const password = AzureGraphService.generatePassword();

    const newUser = {
      displayName,
      accountEnabled: true,
      passwordProfile: {
        password,
        forceChangePasswordNextSignIn: false,
      },
      identities: [
        {
          signInType: 'emailAddress',
          issuer: this.domainName,
          issuerAssignedId: email,
        },
      ],
      passwordPolicies: 'DisablePasswordExpiration',
    };

    return this.#client.api('/users').post(newUser);
  }

  // https://learn.microsoft.com/en-us/graph/api/user-list?view=graph-rest-1.0&tabs=http
  async filterUsers(filter, total = 1) {
    const { value: users } = await this.#client
      .api('/users')
      .header('ConsistencyLevel', 'eventual')
      .filter(filter)
      .count(true)
      .top(total)
      .get();

    return users;
  }

  /* AUXILIAR */
  get domainName() {
    return `${this.#tenantName}.onmicrosoft.com`;
  }

  get atDomain() {
    return `@${this.domainName}`;
  }

  isValidPrincipalName(email) {
    return email.endsWith(this.atDomain);
  }

  formatPrincipalName(email) {
    // can name have @?
    const [name] = email.split('@');

    return `${name}${this.atDomain}`;
  }

  generatePrincipalName(email) {
    if (this.isValidPrincipalName(email)) {
      return email;
    }

    return this.formatPrincipalName(email);
  }

  static fixMailNickname(mailNickname) {
    return mailNickname.replace(/[“(),:;<>@[\] ]/g, '');
  }

  static isValidPassword(password) {
    const passwordRegexp =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@#$%^&*])(?=.{8,})/;
    return new RegExp(passwordRegexp).test(password);
  }

  // Needs caps + number + special char = P9#
  static generatePassword() {
    return `P9#${Math.random().toString(36).slice(2, 7)}`;
  }

  // should not be here
  static extractDomainName(email) {
    const [, domain] = email.split('@');

    return domain;
  }

  static generateRandomIdentifier() {
    return Date.now().toString(32);
  }
}

module.exports = AzureGraphService;
