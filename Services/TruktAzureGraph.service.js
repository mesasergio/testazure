const AzureGraphProvider = require('../Providers/AzureGraph.provider');
const AzureGraphService = require('../Azure/AzureGraph');

const tenantId = process.env.TENANT_ID;
const tenantName = process.env.TENANT_NAME;
const clientId = process.env.CLIENT_ID;
const clientSecret = process.env.CLIENT_SECRET;

const azureGraphProvider = AzureGraphProvider.initWithMiddleware({
  tenantId,
  clientId,
  clientSecret,
});

const azureGraphService = new AzureGraphService(azureGraphProvider, {
  tenantName,
});

module.exports = azureGraphService;
