# Trukt - Senior Code Interview

## Intro
You are being hired as a Senior / Tech Lead.


This role requires you to have mastered syntax, programming structure, and software development principles as well as best practices. You will be expected to uphold a high code standard which includes assisting and directing Mid-level and Junior developers with their code. We also expect for Senior / Tech Leads to understand code quickly and intuitively. It is also expected to understand the intention of the developer, meaning the Senior / Tech Lead should be able to understand why a developer wrote code in a certain way. You are expected to be able to break apart the code, find weaknesses, bad design, and logical errors. You should be able to do this without running the code, by simply looking at the source code and reading it, you should have enough experience that it is second nature to you.

## Task
Included in this code interview is source code that a "Mid-level" developer wrote.

Your task is to do a proper code review and give feedback on the developer's work. This code is taken from our development servers and has already been refactored.

We are looking for candidates that give great criticism and hold a high level of code quality expectation.


You may require to understand some concepts from Microsoft's Cloud Platform and we expect you should be able to do so easily.
